from kafka import KafkaConsumer
import kafka.errors, psycopg2, codecs, json, os

def crack_dbimport():
    try:
        consumer = KafkaConsumer(
            bootstrap_servers=f"{os.environ['KSERVER']}:{os.environ['KPORT']}", 
            key_deserializer = lambda v: json.loads(v.decode('ascii')), 
            value_deserializer = lambda v: json.loads(v.decode('ascii')), 
            enable_auto_commit = True, 
            auto_offset_reset='latest')

        conn = None
        conn = psycopg2.connect(
            host=os.environ['DBSERVER'],
            database=os.environ['DATABASE'],
            user=os.environ['DBUSER'],
            password=os.environ['DBPWD'])

    except kafka.errors.NoBrokersAvailable:
        print("Verbindung fehlgeschlagen")

    except (Exception, psycopg2.DatabaseError) as error:
        print(error)

    else:
        cur = conn.cursor()
        cur.execute('DROP TABLE IF EXISTS testing;')
        conn.commit()      
        cur.execute('CREATE TABLE testing (pk_id SERIAL, key VARCHAR(255), name VARCHAR(255), password VARCHAR(255));')
        conn.commit()

        topic = 'mobile_passwords'

        consumer.subscribe(topics=topic)
        for message in consumer:
            query = "INSERT INTO testing (key, name, password) VALUES ('%s', '%s', '%s');" % (message.key.get('id'), message.value.get('name'), codecs.decode(message.value.get('password'), 'rot_13'))
            cur.execute(query)
            conn.commit()
 
    finally:
        if conn is not None:
            conn.close()
            print('Schließe Datenbankverbindung')        

if __name__ == '__main__':
   crack_dbimport()
