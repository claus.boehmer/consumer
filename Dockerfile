FROM python:slim

ENV DBSERVER=localhost
ENV DBPORT=5432
ENV DATABASE=postgres
ENV DBUSER=consumer
ENV DBPWD=consumer
ENV KSERVER=localhost
ENV KPORT=9092

RUN mkdir -p /kafka
WORKDIR /kafka
COPY requirements.txt consumer.py .
RUN pip install --no-cache-dir -r requirements.txt

CMD ["python", "/kafka/consumer.py"]