# consumer

## Info

Dekodiert Passwörter(rot13) aus den Messages eines Brokers und speichert die Informationen in einer Datenbank.

## Start des Containers

```
podman run -d -e DBSERVER=192.168.86.42 -e DBPORT=5432 -e DATABASE=postgres -e DBUSER=consumer -e DBPWD=consumer -e KSERVER=192.168.86.42 -e KPORT=9092 --name consumer registry.gitlab.com/claus.boehmer/consumer
```